let headers = new Headers();

  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');
  headers.append('Access-Control-Allow-Credentials', 'true');

  headers.append('GET', 'POST', 'OPTIONS');

  fetch("http://127.0.0.1:8000/api/v1.0/categories/", {
      credentials: 'include',
      method: 'GET',
      headers: headers
    })
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(error => console.log('Authorization failed : ' + error.message));