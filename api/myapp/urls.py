from django.urls import path, include
from rest_framework.routers import DefaultRouter


from . import views


router = DefaultRouter()
router.register(r'categories', views.ListRetrieveUpdateDestroyCategory)
router.register(r'posts', views.ListRetrieveUpdateDestroyPost)
router.register(r'mymodels', views.ListRetrieveUpdateDestroyMymodel)

urlpatterns = [
    path('', include(router.urls))
]


