from django.db.models import fields
from myapp.models import *
from rest_framework import serializers



class MyModelSerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MyModel
        fields = ['id', 'title', 'content', 'active']



class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class PostSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'