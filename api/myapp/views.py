from rest_framework import generics
from rest_framework import viewsets

#===============my imports===========================================================
from myapp.models import *
from myapp.serializers import *
#===============end===========================================================



class ListRetrieveUpdateDestroyMymodel(viewsets.ModelViewSet):
    queryset = MyModel.objects.all()
    serializer_class = MyModelSerialiser


class ListRetrieveUpdateDestroyCategory(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ListRetrieveUpdateDestroyPost(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

